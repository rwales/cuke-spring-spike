# README #

### What is this repository for? ###

* Proof of concept for driving Spring MVC tests from Cucumber

### How do I get set up? ###

* run ``mvn jetty:run`` and navigate to ``http://localhost:8080/hello`` to see the app in action.
* run the ``RunCucumber`` class within a Java environment to trigger the cucumber tests.

### Spring Test ###

The step definitions rely heavily on ``spring-test``. Consequently, the responses are 'mocked', not real. There is no real server running here and no real response. All smoke and mirrors.

* TODO: Mockito integration.