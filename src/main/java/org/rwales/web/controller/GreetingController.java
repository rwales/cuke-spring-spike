package org.rwales.web.controller;

import org.rwales.core.service.GreeterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GreetingController {

    @Autowired
    private GreeterService greeterService;

    @RequestMapping("/hello")
    public String sayHello(final Model model, final String name) throws Exception {
        final String prettyName = greeterService.getPrettyName(name);
        model.addAttribute("name", prettyName);
        return "hello";
    }
}
