package org.rwales.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"org.rwales.web"})
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry resourceHandlerRegistry) {
        final ResourceHandlerRegistration resourceHandlerRegistration =
                resourceHandlerRegistry.addResourceHandler("/WEB-INF/view/**/*");
        resourceHandlerRegistration.addResourceLocations(
                "classpath:/META-INF/webapp/WEB-INF/view/");
    }

    @Bean
    public ViewResolver viewResolver() {
        UrlBasedViewResolver _viewResolver =
                new UrlBasedViewResolver();
        _viewResolver.setViewClass(JstlView.class);
        _viewResolver.setPrefix("WEB-INF/view/");
        _viewResolver.setSuffix(".jsp");
        return _viewResolver;
    }

}
