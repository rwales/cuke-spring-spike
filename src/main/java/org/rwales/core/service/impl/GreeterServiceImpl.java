package org.rwales.core.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.rwales.core.service.GreeterService;
import org.springframework.stereotype.Service;

@Service
public class GreeterServiceImpl implements GreeterService {

    @Override
    public String getPrettyName(final String name) {
        return StringUtils.capitalize(StringUtils.lowerCase(name));
    }
}
