package org.rwales.core.service;

public interface GreeterService {

    String getPrettyName(String name);
}
