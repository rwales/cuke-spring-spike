package org.rwales.core;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"org.rwales.core.service"})
public class CoreConfig {
}
