<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Greetings</title>
</head>
<body>
<h1>Greetings!</h1>
<c:choose>
    <c:when test="${empty name}">
        <form action="">
            <label for="name">Please enter your name:</label>
            <input id="name" name="name" type="text" />
            <input type="submit" value="Submit" />
        </form>
    </c:when>
    <c:otherwise>
        Hello <c:out value="${name}"/>!
    </c:otherwise>
</c:choose>

</body>
</html>
