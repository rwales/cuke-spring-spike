Feature: A personalized greeting is given to the user

  Scenario: Personalised greeting is produced
    Given the name "Richard" is provided
    Then the status is OK
    And the name "Richarx" was passed to the model