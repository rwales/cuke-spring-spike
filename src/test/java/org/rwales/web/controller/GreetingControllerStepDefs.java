package org.rwales.web.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.rwales.core.CoreConfig;
import org.rwales.web.WebConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

@WebAppConfiguration
@ContextConfiguration(classes = { CoreConfig.class, WebConfig.class })
public class GreetingControllerStepDefs {

	private MockMvc mockMvc;
	private ResultActions resultActions;

    @Autowired private WebApplicationContext webApplicationCtx;

    @Before
    public void setUp() {
    	mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationCtx).build();
    }
    
    @Given("^the name \"([^\"]*)\" is provided$")
    public void nameIsProvided(final String name) throws Throwable {
        resultActions = mockMvc.perform(get("/hello?name=" + name));
    }

    @Then("^the status is OK$")
    public void statusIsOk() throws Exception {
        resultActions = resultActions.andExpect(status().isOk());
    }

    @And("^the name \"([^\"]*)\" was passed to the model$")
    public void correctNamePassedToModel(final String name) throws Exception {
        resultActions = resultActions.andExpect(model().attribute("name", equalTo(name)));
    }
    
//    @Test
//    public void whenThen() throws Exception {
//    	 mockMvc.perform(get("/hello?name=Richard"))
//    	 	.andExpect(status().isOk())
//    	 	.andExpect(model().attribute("name", equalTo("Richard")));
//    }

}
