package org.rwales.web.controller;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(glue = { "org.rwales.web.controller" })
public class RunCucumber {

}
